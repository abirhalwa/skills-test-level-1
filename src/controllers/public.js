const fs = require('fs');

function csvToJson(csvString) {
  const jsonArray = [];
  //split the string into array 
  const lines = csvString.split(/\r\n|\n|\r/);
  // Extract the keys from the first line
  const keys = lines.shift().split(",");
  // Create objects from the rest of the lines
  for (const line of lines) {
    // convert the current line of values into array
    const values = line.split(",");
    // Create the object
    let obj = {};
    for (let i = 0; i < keys.length; i++) {
      // replace the whitespace in the key name with an underscore 
      const key = keys[i].replace(" ", "_");
      obj[key] = values[i];
    }
    // Push object to array
    jsonArray.push(obj);
  }
  return jsonArray;
}

function getGladiatorsByYear(req, res) {
  try {
    const csvString = fs.readFileSync("gladiators.csv", "utf8");
    const gladiators = csvToJson(csvString);
    const year = parseInt(req.params.year);
    if (isNaN(year)) {
      return res.status(400).json({
        "message": "invalid Parameter Type"
      });
    }
    // Filter the result by the given year
    const result = gladiators.filter((gladiator) => {
      return year >= parseInt(gladiator["first_year"]) && year <= parseInt(gladiator["last_year"]);
    });
    const obj = {result:result}
    res.json(obj);
  } catch {
     return res.status(500).json({
      "message": "Internal Server Error"
    });
  }
}

module.exports = {
  getGladiatorsByYear
};